<?php 

	function wpowo_customize_register($wp_customize){
		//showcase section

		$wp_customize->add_section('showcase', array(
				'title' => __('Showcase', 'wpbootstrap'),
				'description' => sprintf(__('Options for showcase','wp-bootstrap')),
				'priority' => 130

			));

		$wp_customize->add_setting('showcase_heading',array(

				'default' => _x('Custom Bootstrap Wordpress Theme','wp-bootstrap'),
				'type' => 'theme_mod'
			));

		$wp_customize->add_control('showcase_heading',array(

				'label' => __('Heading','wp-bootstrap'),
				'section' => 'showcase',
				'priority' => 0
			));


		$wp_customize->add_setting('showcase_text',array(

				'default' => _x('Custom Bootstrp text','wp-bootstrap'),
				'type' => 'theme_mod'
			));

		$wp_customize->add_control('showcase_text',array(

				'label' => __('Text','wp-bootstrap'),
				'section' => 'showcase',
				'priority' => 1
			));

		$wp_customize->add_setting('btn-url',array(

				'default' => _x('http://test.com','wp-bootstrap'),
				'type' => 'theme_mod'
			));

		$wp_customize->add_control('btn_url',array(

				'label' => __('Button URL','wp-bootstrap'),
				'section' => 'showcase',
				'priority' => 2
			));

		$wp_customize->add_setting('btn_text',array(

				'default' => _x('Read More','wpbootstrap'),
				'type' => 'theme_mod'
			));

		$wp_customize->add_control('showcase_heading',array(

				'label' => __('Button Text','wpbootstrap'),
				'section' => 'showcase',
				'priority' => 3
			));

	}

	add_action('customize_register','wpowo_customize_register' );

?>