<?php

// Register Custom Navigation Walker
require_once('wp_bootstrap_navwalker.php');


function wpowo_theme_setup(){
	add_theme_support('post-thumbnails' );

	register_nav_menus( array(
    'primary' => __( 'Primary Menu', 'THEMENAME' ),
) );

	add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );

}

add_action('after_setup_theme','wpowo_theme_setup');


// this functuino is to limit the length of content displayed

function set_excerpt_length(){
	return 20;
}
add_filter('excerpt_length','set_excerpt_length');

// widget locations

function wpowo_init_widgets($id){
		   /**
			* Creates a sidebar
			* @param string|array  Builds Sidebar based off of 'name' and 'id' values.
			*/
			$owodata = array(
				'name'          => __( 'Owo Sidebar', 'theme_text_domain' ),
				'id'            => 'owo-sidebar',
				'description'   => 'This widget lists previous widgets',
				'class'         => '',
				'before_widget' => '<div class="sidebar-module">',
				'after_widget'  => '</div>',
				'before_title'  => '<h4 class="widgettitle">',
				'after_title'   => '</h4>'
			);
		
			register_sidebar( $owodata );


			$owobox = array(
				'name'          => __( 'Box 1', 'theme_text_domain' ),
				'id'            => 'owo-box1',
				'description'   => 'This is box1 widgets',
				'class'         => '',
				'before_widget' => '<div class="box">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
			);
		
			register_sidebar( $owobox );

			$owobox2 = array(
				'name'          => __( 'Box 2', 'theme_text_domain' ),
				'id'            => 'owo-box2',
				'description'   => 'This is box2 widgets',
				'class'         => '',
				'before_widget' => '<div class="box">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
			);
		
			register_sidebar( $owobox2 );

			$owobox3 = array(
				'name'          => __( 'Box 3', 'theme_text_domain' ),
				'id'            => 'owo-box3',
				'description'   => 'This is box3 widgets',
				'class'         => '',
				'before_widget' => '<div class="box">',
				'after_widget'  => '</div>',
				'before_title'  => '<h3>',
				'after_title'   => '</h3>'
			);
		
			register_sidebar( $owobox3 );
		
}

add_action('widgets_init','wpowo_init_widgets');

// include customizer file here
require get_template_directory(). '/inc/customizer.php';