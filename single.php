<?php get_header(); ?>

    <div class="row">
    
        <div class="col-sm-8 blog-main">
          <?php if(have_posts()):?>
            <?php while(have_posts()) : the_post();?>
              <!-- the previous content within the while loop has been copied to another location -->
             <?php get_template_part('content', get_post_format( ));?>
          <?php endwhile;?>
        <?php else: ?>
          <p><?php __('No Posts Found');?></p>
        <?php endif; ?>

          <nav>
            <ul class="pager">
              <li><a href="#">Previous</a></li>
              <li><a href="#">Next</a></li>
            </ul>
          </nav>

        </div><!-- /.blog-main -->

        <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
          <div class="sidebar-module sidebar-module-inset">
            <?php if(is_active_sidebar('owo-sidebar')):?>
            <?php dynamic_sidebar('owo-sidebar');?>
          <?php endif;?>
        </div><!-- /.blog-sidebar -->

      </div><!-- /.row -->

    </div><!-- /.container -->
<?php get_footer(); ?>
